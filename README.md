# CloudFlare backup tool

`backup-cloudflare` is saving your Cloudflare domain records and settings
to a directory in JSON format.

## Installation

```sh
pip install git+https://bitbucket.org/svitkovskiy/backup-cloudflare.git
```

## Usage

```
usage: backup-cloudflare [-h] [--destdir DESTDIR] [--syslog] [--no-stderr]
                         [--debug]

Backup Cloudflare settings

optional arguments:
  -h, --help            show this help message and exit
  --destdir DESTDIR, -d DESTDIR
                        Configuration file. Default: ~/.cfbackup
  --syslog, -s          Log to syslog.
  --no-stderr, -n       Do not log to stderr.
  --debug, -v           Enable full debug
```

* `--destdir` defines the direcotry to save Cloudflare domain information, By default, `~/.cfbackup` is used.
* `--syslog` saves all logs to syslog
* `--no-stderr` siables stderr logging (without `--syslog` no logging we be done ever)
* `--debug` guess what

## Configuration

Since the official [python-cloudflare](https://github.com/cloudflare/python-cloudflare) library is used,
the configuration is [standard](https://github.com/cloudflare/python-cloudflare#providing-cloudflare-username-and-api-key):

1. Setting `CF_API_KEY` environment variable
2. Using `~/.cloudflare/cloudflare.cfg` ini file:

```ini
[CloudFlare]
token = 00000000000000000000000000000000
```

## TODO

1. Be able to restore settings back to Cloudflare
