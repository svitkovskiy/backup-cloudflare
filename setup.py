#!/usr/bin/env python
"Setup file for backup_cloudflare"

from setuptools import setup

setup(
    name='backup_cloudflare',
    version='0.8',
    description='Tool to backup Cloudflare settings',
    scripts=['backup-cloudflare'],
    console=['backup-cloudflare'],
    install_requires=['cloudflare==2.6.3'],
)
